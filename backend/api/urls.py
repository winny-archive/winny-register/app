from rest_framework.authtoken.views import obtain_auth_token
from .routers import router
from django.urls import path, include
from .views import ping, logout

urlpatterns = [
    path('', include(router.urls)),
    path('login/', obtain_auth_token),
    path('logout/', logout),
    path('ping/', ping),
]
