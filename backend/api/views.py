from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.serializers import TransactionSerializer, ProductSerializer
from api.models import User, Transaction, Product
from django.db.models import Sum
import datetime


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


@api_view(['POST'])
def logout(request):
    request.auth.delete()
    return Response({'logged_out': True})


@api_view()
def ping(requset):
    return Response({'pong':  datetime.datetime.utcnow()})
