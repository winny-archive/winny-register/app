#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t
#+OPTIONS: broken-links:nil c:nil creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t
#+OPTIONS: tasks:t tex:t timestamp:t title:t toc:t todo:t |:t
#+TITLE: Cash Register Sample Application
#+DATE: <2020-08-20 Thu>
#+AUTHOR:
#+EMAIL:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.3 (Org mode 9.1.9)
#+startup: indent
* Getting started

* License

* Requirements

Create a cash register simulator application.  The user is a cashier, using the
application to process a customer's transaction.

** Goals

- User can enter a series of prices for products that are purchased

- Application calculates the total cost of the products

- User then enters the amount received from customers and application
  calculates the amount of change to be given.

** Possible Expansiosn

- Application tells the user which bills and coins to give the customer for
  their change (ex: 2 $1, 2 quarters, 3 pennies)

- Rather than enter prices, the system has an inventory of products that
  already have the prices entered.  User then picks the product from the
  inventory list and the quantity.
  + Add-on: Flag some items as tax-exempt and have the application
    determine the proper tax amount.
  + Add-on: Create a screen for inventory management.  The user of the
    application can use this screen to manage what products they sell and
    what the price of each is.
  + Applications shows a list of products that are purchased during the
    transaction.  The user can go back and update the purchased quantity
    of a product or remove it from the order.

- Store products and/or transaction history in a database or file for
  permanent storage.
  + Develop a screen that allows the store manager to view their
    inventory.  They can specify how many of a given product they have
    and purchases of that product will reduce this value.
  + Develop a way to track daily sales totals.

- Support discount codes or coupons.

- Application can put together a receipt to give to the user (Output to a
  text file or other output)

- Any other expansions related to the use case of the application
