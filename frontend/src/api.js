const superagent = require('superagent')

const endpoint = process.env.api_endpoint

function login(username, password) {
  return superagent
    .post(endpoint + 'login/')
    .accept('json')
    .send({username: username, password: password})
}

function logout(token) {
  return superagent
    .post(endpoint + 'logout/')
    .accept('json')
    .set('Authorization', 'Token ' + token)
}

function ping(token) {
  return superagent
    .get(endpoint + 'ping/')
    .accept('json')
    .set('Authorization', 'Token ' + token)
}

exports.login = login;
exports.logout = logout;
exports.ping = ping;
exports.endpoint = endpoint;
